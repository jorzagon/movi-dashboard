# Pendientes Dahsboard

 - [x] TPR y TPRI de WhatsApp
 - [x] TPR y TPRI de Facebook
 - [x] TPR y TPRI de WhatsApp Técnica y Comercial.
 - [x] TPR y TPRI de Facebook Técnica y Comercial.
 - [x] TPRC de Facebook y WhatsApp.
 - [x] TPRC de Facebook Comercial y Técnica.
 - [x] TPRC de WhatsApp Comercial y Técnica.
 - [x] Demanda total Facebook y WhatsApp.
 - [x] Demanda Facebook Técnica y Comercial.
 - [x] Demanda WhatsApp Técnica y Comercial.
 
 
 # Métricas Ventas
 ## Excel de Gonza
 
 - [ ] Mostrar el gráfico de **Cumplidas vs. Mes anterior**
 - [x] De la celda **Gral y Cuartiles**, de cada proveedor, mostrar fila 15 y 16 (Migraciones).
 - [x] De la celda **Gral y Cuartiles**, de cada proveedor, mostrar fila 18 (Portabilidad).
 - [ ] De la celda **Gral y Cuartiles**, de cada proveedor, fila 20
 - [ ] Gráfico combinadode **Migracios**, **Portabilidad** y **FTTH**.
