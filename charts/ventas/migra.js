import { migraCtrl, migraFull, portabilidad, ftth } from "../data.js";


const datas = [

    Object.entries(migraCtrl).map(function (data) {
        return { name: data[0], value: data[1] }
    }),

    Object.entries(migraFull).map(function (data) {
        return { name: data[0], value: data[1] }
    }),

    Object.entries(portabilidad).map(function (data) {
        return { name: data[0], value: data[1] }
    }),

    Object.entries(ftth).map(function (data) {
        return { name: data[0], value: data[1] }
    })
]


const valorDiccionarios = []
const nombreDiccionarios = []
// Recorre la variable datas y obtiene el nombre y el valor de cada objeto
datas.forEach(function (dictionary) {
    // Obtener todos los valores
    const valores = dictionary.map(function (item) {
        return item.value;
    });

    // Obtener los nombres de los índices
    const nombres = dictionary.map(function (item) {
        return item.name;
    });

    valorDiccionarios.push(valores);
    nombreDiccionarios.push(nombres);
});


let colores = ['#0000A3', '#0067B3', '#40B0DF', '#FFD53D']
// Recorre los colores de la lista 'colores'
const color = (parametro) => {
    return colores[parametro.dataIndex]
}


// Inicializa el gráfico Migraciones Control
let chartMigraCtrl = echarts.init(document.getElementById('migra_control'));

let optionControl = {
    legend: {
        top: 'bottom'
    },
    title: {
        text: 'Migraciones Control',
        left: 'center'
    },
    grid: {
        left: '3%',
        right: '4%',
        bottom: '3%',
        containLabel: true
    },

    series: [
        {
            label: {
                formatter: '{c}',
                position: 'inside'
            },
            name: 'Migraciones',
            type: 'pie',
            radius: [40, 90],
            //'roseType' brinda el estilo de rosa al gráfico
            //roseType: 'area',
            itemStyle: {
                borderRadius: 2
            },
            data: datas[0]
        }
    ]
}

optionControl && chartMigraCtrl.setOption(optionControl)


// Inicializa el gráfico Migraciones Full
let chartMigraFull = echarts.init(document.getElementById('migra_full'));

let optionFull = {
    legend: {
        top: 'bottom'
    },
    title: {
        text: 'Migraciones Full',
        left: 'center'
    },
    grid: {
        left: '3%',
        right: '4%',
        bottom: '3%',
        containLabel: true
    },

    series: [
        {
            label: {
                formatter: '{c}',
                position: 'inside'
            },
            name: 'Migraciones',
            type: 'pie',
            radius: [40, 90],
            //'roseType' brinda el estilo de rosa al gráfico
            //roseType: 'area',
            itemStyle: {
                borderRadius: 2
            },
            data: datas[1]
        }
    ]
}

optionFull && chartMigraFull.setOption(optionFull)


// Inicializa el gráfico Portabilidades
let chartPorta = echarts.init(document.getElementById('portabilidad'));

let optionPorta = {
    legend: {
        top: 'bottom'
    },
    title: {
        text: 'Portabilidades',
        left: 'center'
    },
    grid: {
        left: '3%',
        right: '4%',
        bottom: '3%',
        containLabel: true
    },

    series: [
        {
            label: {
                formatter: '{c}',
                position: 'inside'
            },
            name: 'Portabilidad',
            type: 'pie',
            radius: [40, 90],
            //'roseType' brinda el estilo de rosa al gráfico
            //roseType: 'area',
            data: datas[2],
            itemStyle: {
                borderRadius: 2
            }
        }
    ]
}

optionPorta && chartPorta.setOption(optionPorta)


// Inicializa el gráfico de FTTH
let chartFTTH = echarts.init(document.getElementById('ftth'));

let optionFTTH = {
    title: {
        text: 'FTTH',
        left: 'left'
    },
    grid: {
        left: '3%',
        right: '4%',
        bottom: '3%',
        containLabel: true
    },

    tooltip: {
        trigger: 'item'
      },

    series: [
        {
            label: {
                show: true,
                formatter: '{b}: {c}'
            },
            labelLine: {
                show: true
            },
            name: 'FTTH',
            type: 'pie',
            radius: [40, 90],
            data: datas[3],
            itemStyle: {
                // color: color,
                borderRadius: 2
            }
        }
    ]
}

optionFTTH && chartFTTH.setOption(optionFTTH)


// Inicializa el gráfico combinado de Migraciones Control, Full, Portabilidades y FTTH
let chartCombinado = echarts.init(document.getElementById('combinado'));

let optionCombinado = {
    grid: {
        left: '3%',
        right: '4%',
        bottom: '3%',
        containLabel: true
    },
    tooltip: {
        trigger: 'item'
      },
    legend: {
        data: nombreDiccionarios[0]
    },
    xAxis: {
            type: 'category',
            data: ['Migra Control', 'Migra Full', 'Portabilidad', 'FTTH']
        },
    yAxis: {
            type: 'value'
        },
    series: [
        {
            name: nombreDiccionarios[0][0],
            type: 'bar',
            data: [valorDiccionarios[0][0], valorDiccionarios[1][0], valorDiccionarios[2][0], valorDiccionarios[3][0]],
            label: { show: true }
        },
        {
            name: nombreDiccionarios[0][1],
            type: 'bar',
            data: [valorDiccionarios[0][1], valorDiccionarios[1][1], valorDiccionarios[2][1], valorDiccionarios[3][1]],
            label: { show: true }
        }, 
        {
            name: nombreDiccionarios[0][2],
            type: 'bar',
            data: [valorDiccionarios[0][2], valorDiccionarios[1][2], valorDiccionarios[2][2], valorDiccionarios[3][2]],
            label: { show: true }
        },
        {
            name: nombreDiccionarios[0][2],
            type: 'bar',
            data: [valorDiccionarios[0][3], valorDiccionarios[1][3], valorDiccionarios[2][3], valorDiccionarios[3][3]],
            label: { show: true }
        }
    ]
}

optionCombinado && chartCombinado.setOption(optionCombinado)
